# Hack4Rice_Genomic_Prediction_Challenge

Welcome!
This repository will host the data for Genomic Prediction challenge.

## Data

The training dataset contains the following columns:

 Column  | Meaning
 ------- | -------
 ID      | Sequential ID of the sample
 group   | Rice variety group based on genotype
 GRWT100 | 100-grain weight
 GRWD    | Grain width
 GRLT    | Grain length
 LWD     | Leaf width
 Seedling_height | Seedling height
 X28751_G | Genotype column ( SNP at position 28751 )
 ...  | ...
 X373240584_T | Genotype column


Columns 3-7 are the phenotypes to be predicted from the genotype columns.

Colum 2 (variety group) is an additional info about the sample that may be optionally used for data exploration and modeling.

However it is not part of the test data, so cannot be used as a feature.

#### Genotype data

Genotype data are numerically encoded. 
Each genotype column correspond to a particular location in the genome (DNA) 
and particular mutations at that location (both encoded in the column name). 
The values describe the number of copies of the mutation present in the sample's. (Could be 0,1, or 2).

Each column describes the genotype of each sample at particular genomic position
in terms of the number of copies of particular mutation (0,1, or 2).
The original data had missing values, which were filled with mean value for the column (these are seen as non-integer values). 
The means were computed over all available samples (some are not present in the training data), thus it may differ from the mean in training set.


### Test data

The test dataset will only contain an ID column and genotype columns (ID, X28751_G, ...,  X373240584_T)

The submission should be a csv file with columns (ID,  GRWT100, ...,  Seedling_height) 
that contain predicted values of the phenotypes.

### Baseline

The models will be compared against each other and against baseline, which is a penalized regression model (Elastic Net with particular l1_ratio and alpha).


